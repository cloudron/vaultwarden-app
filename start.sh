#!/bin/bash

set -eu

mkdir -p /run/vaultwarden

# Vaultwarden sources from environment. The admin page, when visited, also generates config.json which overrides
# the env vars - https://github.com/dani-garcia/vaultwarden/wiki/Configuration-overview . Some values are however
# readonly and can only be set as env var. We use config.json whenever possible to keep things in sync
if [[ ! -f /app/data/env.sh ]]; then
    cat <<EOT > /app/data/env.sh
# Note that Vaultwarden has an admin UI to edit most config variables (config.json).
# Only use this file to export "readonly" config variables - https://github.com/dani-garcia/vaultwarden/wiki/Configuration-overview

export LOG_LEVEL=info
EOT

    # https://github.com/dani-garcia/vaultwarden/wiki/Using-the-MariaDB-%28MySQL%29-Backend#foreign-key-errors-collation-and-charset
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "ALTER DATABASE CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
fi

source /app/data/env.sh

echo "=> Exporting env vars expected by Vaultwarden"
export DATABASE_URL=$CLOUDRON_MYSQL_URL
export ENABLE_DB_WAL=false
export LOG_FILE=/run/vaultwarden/vaultwarden.log
export WEBSOCKET_ENABLED=true
export DATA_FOLDER=/app/data
export CONFIG_FILE=/app/data/config.json # defaults to DATA_FOLDER/config.json but being explicit

# these are used by the Rocket web framework. apache proxies to this
export ROCKET_ENV=staging
export ROCKET_PORT=3000
export ROCKET_WORKERS=10

export RUST_BACKTRACE=1 # used by rust

[[ ! -f /app/data/config.json ]] && cp /app/pkg/config.json.template /app/data/config.json

cat $CONFIG_FILE |
    jq ".domain = \"${CLOUDRON_APP_ORIGIN}\"" | \
    jq ".ip_header = \"X-Forwarded-For\"" |
    sponge $CONFIG_FILE

# email
cat $CONFIG_FILE |
    jq ".smtp_host = \"${CLOUDRON_MAIL_SMTP_SERVER}\"" | \
    jq ".smtp_from = \"${CLOUDRON_MAIL_FROM}\"" | \
    jq ".smtp_from_name = \"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Vaultwarden}\"" | \
    jq ".smtp_port = ${CLOUDRON_MAIL_SMTP_PORT}" | \
    jq ".smtp_username = \"${CLOUDRON_MAIL_SMTP_USERNAME}\"" | \
    jq ".smtp_password = \"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" | \
    jq ".smtp_auth_mechanism = \"Plain\"" | \
    jq ".smtp_security = \"off\"" | \
    jq ".smtp_accept_invalid_certs = true" | \
    jq ".smtp_accept_invalid_hostnames = true" | \
    sponge $CONFIG_FILE

# admin key
if [[ "$(jq -r .admin_token ${CONFIG_FILE})" == "" ]]; then
    echo "=> Generating new admin token"
    admin_token=$(pwgen -1 48 -s)
    jq ".admin_token = \"$admin_token\"" ${CONFIG_FILE} | sponge ${CONFIG_FILE}
fi

chown -R cloudron:cloudron /run/vaultwarden /app/data

echo "=> Starting supervisord"
rm -f /run/apache2/apache2.pid
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Vaultwarden

