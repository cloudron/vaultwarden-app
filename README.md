# vaultwarden-app

A Cloudron deployment for [Vaultwarden](https://github.com/dani-garcia/vaultwarden) that includes MySQL support .

## Building

### The Dockerfiles

The main `Dockerfile` in this repo is actually a symblink pointing to a proper `Dockerfile` that will be used for building.

There are two real Dockerfiles in this repo. One pulls Vaultwarden from Docker Hub and extracts the binary. The other will actually build it from source. It used to be required to build from source for MySQL integration, but it is no longer necessary. Should you wish to have more control over the build process, or enable any other build time features in the future, you should use `Dockerfile.multi-stage`. You can easily switch between them by changing the symblink for `Dockerfile`.

### The build process

A `Makefile` has been added to simplify the interaction with Cloudron. More detailed instructions exist within the `Makefiel` itself, but to build, push, and install on your Cloudron you should only need to do `make push install` or `make push update`.

You will have to be previously authenticated with both Docker Hub and your Cloudron instance via the Cloudron CLI.
