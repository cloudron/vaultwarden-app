#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'test@cloudron.io';
    const PASSWORD = 'Changeme123#12345';
    const ITEM_NAME = 'Test Item';

    let app, browser;

    before(async function () {
        await startBrowser();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function startBrowser() {
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        await startBrowser();
    }

    async function signup() {
        await browser.get(`https://${app.fqdn}/#/register`);
        await waitForElement(By.id('register-form_input_email'));
        await browser.findElement(By.id('register-form_input_email')).sendKeys(EMAIL);
        await browser.findElement(By.id('register-form_input_name')).sendKeys('Cloudron Test');
        await browser.sleep(4000);
        await browser.findElement(By.id('register-form_input_master-password')).sendKeys(PASSWORD);
        await browser.findElement(By.id('register-form_input_confirm-master-password')).sendKeys(PASSWORD);
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//button[@buttontype="primary"]')).click();
        await browser.sleep(3000);
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/#/login`);

        await waitForElement(By.xpath('//input[@id="login_input_email" or @formcontrolname="email"]'));
        await browser.findElement(By.xpath('//input[@id="login_input_email" or @formcontrolname="email"]')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        await browser.findElement(By.xpath('//input[@id="login_input_email" or @formcontrolname="email"]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//button[@buttontype="primary"]')).click();

        await waitForElement(By.xpath('//input[@id="login_input_master-password" or @formcontrolname="masterPassword"]'));
        await browser.findElement(By.xpath('//input[@id="login_input_master-password" or @formcontrolname="masterPassword"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@buttontype="primary"]')).click();
        await waitForElement(By.xpath('//a[contains(.,"Vaults")]'));
    }

    async function logout() {
/*
        // get over the lock first
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]'));
        await browser.findElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//a[contains(.,"Vaults")]'));
        await browser.findElement(By.xpath('//button//img[@title="Cloudron Test"]')).click(); // the first 'button' has no meaning and only here for reference since // is docroot
        await waitForElement(By.xpath('//button//i[contains(@class, "bwi-sign-out")]'));
        await browser.findElement(By.xpath('//button//i[contains(@class, "bwi-sign-out")]')).click();

        await waitForElement(By.xpath('//input[@id="login_input_email" or @formcontrolname="email"]'));
*/
        await clearCache();
    }

    async function createItem() {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]'));
        await browser.findElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(5000);
        await waitForElement(By.xpath('//a[contains(.,"Vaults")]'));
        await browser.findElement(By.xpath('//button[@buttontype="primary" and not(@aria-haspopup="menu")]')).click();
        await waitForElement(By.id('name'));
        await browser.findElement(By.id('name')).sendKeys(ITEM_NAME);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath(`//button[contains(text(), "${ITEM_NAME}")]`));
    }


    async function itemExists() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]'));
        await browser.findElement(By.xpath('//input[@id="masterPassword" or @formcontrolname="masterPassword"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[contains(.,"Vaults")]'));
        await waitForElement(By.xpath(`//button[contains(text(), "${ITEM_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can signup', signup);
    it('can login', login);
    it('can create item', createItem);

    it('item exists', itemExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app - update', async function () {
        execSync('cloudron install --appstore-id com.github.bitwardenrs --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can signup', signup);
    it('can login', login);
    it('can create item', createItem);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can update', async function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
