FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# The required rust version is installed by cargo during bitwarden build
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup-init.sh \
    && chmod +x rustup-init.sh \
    && ./rustup-init.sh -y --profile minimal && \
    rm rustup-init.sh

ENV PATH /root/.cargo/bin:$PATH

# Note that these two hand in hand. Wait for the backend to list the vault version
# renovate: datasource=github-releases depName=dani-garcia/vaultwarden versioning=semver
ARG VAULTWARDEN_VERSION=1.33.2

# https://github.com/dani-garcia/bw_web_builds/releases/
ARG WEB_VAULT_VERSION=2024.6.2

RUN mkdir /src && \
    curl -L "https://github.com/dani-garcia/bitwarden_rs/archive/${VAULTWARDEN_VERSION}.tar.gz" | tar zxf - --strip-components 1 -C /src && \
    cd /src && \
    cargo build --features mysql --release && \
    mv /src/target/release/vaultwarden /app/code/vaultwarden && \
    rm -rf /src

RUN WEBVAULT_VERSION=$(curl https://raw.githubusercontent.com/dani-garcia/vaultwarden/refs/tags/${VAULTWARDEN_VERSION}/docker/DockerSettings.yaml | yq ".vault_version") && \
    echo "Packaging with Webvault ${WEBVAULT_VERSION}" && \
    curl -L "https://github.com/dani-garcia/bw_web_builds/releases/download/${WEBVAULT_VERSION}/bw_web_${WEBVAULT_VERSION}.tar.gz" | tar zxf -

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN a2disconf other-vhosts-access-log
COPY apache/vaultwarden.conf /etc/apache2/sites-enabled/vaultwarden.conf
RUN a2enmod proxy proxy_http proxy_wstunnel rewrite headers
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY config.json.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

