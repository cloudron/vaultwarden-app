### Overview

This is the Rust implementation of the Bitwarden backend, *not* the official server backend, but fully compatible with the Client apps.

Vaultwarden is a self-hosted password manager. It allows you to store and manage your passwords, credit cards, and other private information in a secure way while still allowing you to access it from your browser, phone, or desktop.

### Client Apps

The official client apps from bitwarden.com are all supported. In fact the webfrontend, packaged together with this app, is also the official one.

Clients can be downloaded at [bitwarden.com](https://bitwarden.com/#download)

Those apps are a lot of effort to maintain, so please consider supporting the upstream project.
