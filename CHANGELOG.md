[0.1.0]
* Initial version

[0.1.3]
* update to upstream 1.9.0

[0.1.4]
* update to upstream 1.9.1

[0.2.0]
* update to upstream 1.13.1
* prepare for official release by adding mysql and optional ldap integration

[1.0.0]
* Initial release
* Bitwardenrs 1.14.2

[1.0.1]
* Initial release
* Bitwardenrs 1.14.2

[1.1.0]
* Use latest base image 2.0.0
* Run bitwarden as non-root user
* Enable bitwarden logs

[1.2.0]
* Revert update of vault 2.14.0

[1.3.0]
* Update Bitwardenrs to 1.15.0
* [Full changelog](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.15.0)
* Added support for soft deletion of items (trash functionality)
* Redesigned admin page
* Updated web vault to 2.14
* Added IP address to the logs on TOTP failure, alowing fail2ban use
* Some email and domain whitelist fixes
* Fixed issue deleting notes in PostgreSQL
* Updated dependencies and other bug fixes

[1.3.1]
* Update BitwardenRS to 1.15.1
* [Full changelog](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.15.1)
* Fixed error when cloning attachments with ciphers, note that attachments are not cloned
* Fixed version check when a commit hasn't been made since the last release
* Added openssl extern crate to fix some builds
* Updated admin page, added attachments count per user and users count per organization and fixed issue with DNS not resolving

[1.4.0]
* Update BitwardenRS to 1.16.0
* Update web vault to 2.15.1
* [Full changelog](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.16.0)
* Add support for hiding passwords in a collection
* Add option to set name during HELO in email settings
* Add startup script to support init operations
* Use local time in email notifications for new device logins
* Updated dependencies and included web vault
* Removed unstable dependencies in preparation for rocket stable

[1.4.1]
* Update BitwardenRS to 1.16.1
* Log timestamps with milliseconds by default and added option LOG_TIMESTAMP_FORMAT to customize the format

[1.4.2]
* Update BitwardenRS to 1.16.2
* Fixed issue unlocking vault in the desktop client.
* Added back arm32v6 tag, because docker fails to select that image in ARMv6 devices.
* Fixed websocket notifications when sending an item to the trash.

[1.4.3]
* Update BitwardenRS to 1.16.3
* Fixed mysql and postgresql releases not building correctly
* Added support for restricting org creation to certain users: Examples
* Syncronized global_domains.json with upstream

[1.4.4]
* Update BitwardenRS to 1.17.0
* Update webvault to 2.16.1
* Sessions are properly invalidated now when changing email, password or kdf parameters.
* Items are not shown to organization admins in their user view when they don't have their collection selected. Note that they still appear in the organization view.
* Favorite status in organization items is now tracked at the user level.
* Updated dependencies and synced global domains file with upstream.

[1.5.0]
* Update BitwardenRS to 1.18.0
* [Full changelog](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.18.0)
* Users can be enabled/disabled from the admin panel.
* Implemented manager role.
* Now cipher updates are validated when they provide a revision date, which will prevent multiple clients from overwriting each other's changes.
* Updated web vault to 2.17.1.

[1.6.0]
* Update BitwardenRS to 1.19.0
* Update web vault to 2.18.1
* Admin UI: Added diagnostic and debug information.
* Admin UI: Added option to sort users by date.
* Admin UI: Added ability to modify a user's type in an organization and to delete the whole organization.
* Added support for the Personal Ownership policy, which when enabled disables the use of the personal vault to non-admin users of an organization.
* Synced global domains data with upstream.

[1.6.1]
* Update to base image v3

[1.6.2]
* Update BitwardenRS to 1.20.0
* Update web vault to 2.19.0
* Implemented Send functionality
* Updated web vault to 2.19.0
* CORS fixes
* Updated diagnostics page with more info
* Updated dependencies

[1.6.3]
* Set `configurePath` in the manifest

[1.7.0]
* BitwardenRS is now Vaultwarden
* Update Vaultwarden to 1.22.0
* Added sends_allowed option to disable Send functionality.
* Added support for hiding the senders email address.
* Added Send options policy.
* Added support for password reprompt.
* Switched to the new attachment download API.
* Send download links use a token system to limit their downloads.
* Updates to the icon fetching.
* Support for webauthn.
* The admin page now shows which variables are overridden.
* Updated dependencies and docker base images.
* Now RSA keys are generated with the included openssl instead of calling to the openssl binary.
* The web vault doesn't require accepting the terms are conditions now, which weren't applicable for a self hosted server.

[1.7.1]
* Update Vaultwarden to 1.22.1
* Added sends_allowed option to disable Send functionality.
* Added support for hiding the senders email address.
* Added Send options policy.
* Added support for password reprompt.
* Switched to the new attachment download API.
* Send download links use a token system to limit their downloads.
* Updates to the icon fetching.
* Support for webauthn.
* The admin page now shows which variables are overridden.
* Updated dependencies and docker base images.
* Now RSA keys are generated with the included openssl instead of calling to the openssl binary.
* The web vault doesn't require accepting the terms are conditions now, which weren't applicable for a self hosted server.

[1.7.2]
* Update Vaultwarden to 1.22.2
* Updated web vault to 2.21.1.
* Enforce 2FA policy in organizations.
* Protect send routes against a possible path traversal attack.
* Disable show_password_hint by default, it still can be enabled in the admin panel or with environment variables.
* Disable user verification enforcement in Webauthn, which would make some users unable to login.
* Fix issue that wouldn't correctly delete Webauthn Key.
* Added Edge extension support for Webauthn.

[1.7.3]
* Fix logging of client IP address
* Set IP_HEADER config var

[1.7.4]
* Update Vaultwarden to 1.23.0
* Update web vault to 2.23.0

[1.7.5]
* Update Vaultwarden to 1.23.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.23.1)
* Add email notifications for incomplete 2FA logins by @jjlin in #2067
* Fix conflict resolution logic for read_only and hide_passwords flags by @jjlin in #2073
* Fix missing encrypted key after emergency access reject by @jjlin in #2078
* Macro recursion decrease and other optimizations by @BlackDex in #2084
* Enabled trust-dns and some updates. by @BlackDex in #2125
* Update web vault to 2.25.0

[1.7.6]
* Update base image to 3.2.0
* Better apache config to not cause hostname warnings

[1.8.0]
* Rework configuration to use config.json instead of env variables

[1.8.1]
* Update Vaultwarden to 1.24.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.24.0)
* Add support for API keys by @jjlin in #2245
* Basic ratelimit for user login (including 2FA) and admin login by @dani-garcia in #2165
* Upgrade Feature-Policy to Permissions-Policy by @iamdoubz in #2228
* Set Expires header when caching responses by @RealOrangeOne in #2182
* Increase length limit for email token generation by @jjlin in #2257
* Small changes to icon log messages. by @BlackDex in #2170
* Bump rust version to mitigate CVE-2022-21658 by @dscottboggs in #2255
* Fixed #2151 by @BlackDex in #2169
* Fixed issue #2154 by @BlackDex in #2194
* Fix issue with Bitwarden CLI. by @BlackDex in #2197
* Fix emergency access invites for new users by @BlackDex in #2217
* Sync global_domains.json by @jjlin in #2156

[1.8.2]
* Update web vault to 2.24.0
* [Full changelog](https://github.com/bitwarden/web/releases/tag/v2.24.0)
* various bug fixes

[1.8.3]
* Update Web vault to 2.28.0

[1.8.4]
* Update Web Vault to 2.28.1

[1.9.0]
* Update Vaultwarden to 1.25.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.25.0)
* Updated included web vault to v2.28.1
* Update Rocket to 0.5 and async, and compile on stable by @dani-garcia in #2276
* Update async to prepare for main merge + several updates by @BlackDex in #2292
* Add IP address to missing/invalid password message for Sends by @jaen in #2313
* Add support for custom .env file path by @TinfoilSubmarine in #2315
* Added autofocus to pw field on admin login page by @taylorwmj in #2328
* Update login API code and update crates to fix CVE by @BlackDex in #2354

[1.9.1]
* Update Vaultwarden to 1.25.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.25.1)
* Updated included web vault to version 2022.6.2 by @dani-garcia
* Sync global_domains.json by @jjlin in #2555
* Add TMP_FOLDER to .env.template by @fox34 in #2489
* Allow FireFox relay in CSP. by @BlackDex in #2565
* Fix hidden ciphers within organizational view. by @BlackDex in #2567
* Add password_hints_allowed config option by @jjlin in #2586
* Fall back to move_copy_to if persist_to fails while saving uploaded files. by @ruifung in #2605

[1.9.2]
* Update Vaultwarden to 1.25.2
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.25.2)
* Fix persistent folder check within containers by @BlackDex in #2631
* Mitigate attachment/send upload issues by @BlackDex in #2650
* Fix issue with CSP and icon redirects by @BlackDex in #2624
* Update build workflow for CI by @BlackDex in #2632

[1.10.0]
* Update Vaultwarden to 1.26.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.26.0)
* Updated web vault to v2022.10.0
* Fix uploads from mobile clients (and dep updates) by @BlackDex in #2675
* Update deps and Alpine image by @BlackDex in #2665
* Add support for send v2 API endpoints by @BlackDex in #2756
* External Links | Optimize behavior by @Fvbor in #2693
* Add Org user revoke feature by @BlackDex in #2698
* Change the handling of login errors. by @BlackDex in #2729
* Added support for web-vault v2022.9 by @BlackDex in #2732
* add not_found catcher for 404 errors by @stefan0xC in #2768
* Fix issue 2737, unable to create org by @BlackDex in #2738

[1.10.1]
* Update Web Vault to 2022.10.2

[1.10.2]
* Update application logo
* Use new SMTP setting variables to avoid deprecation warning

[1.10.3]
* Update Web Vault to 2022.11.2

[1.11.0]
* Update Vaultwarden to 1.27.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.27.0)
* Group support | applied .diff by @MFijak in #2846
* Add Organizational event logging feature by @BlackDex in #2868
* Updated web vault to 2022.12.0 by @dani-garcia
* Update diesel to 2.0.2 by @dani-garcia in #2724
* Limit Cipher Note encrypted string size by @BlackDex in #2945
* fix invitations of new users when mail is disabled by @stefan0xC in #2773
* attach images in email by @stefan0xC in #2784
* allow registration without invite link by @stefan0xC in #2799
* Fix master password hint update not working. by @BlackDex in #2834

[1.12.0]
* Update Vaultwarden to 1.28.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.28.0)
* The project has changed license to the AGPLv3. If you're hosting a Vaultwarden instance, you now have a requirement to distribute the Vaultwarden source code to your users if they request it. The source code, and any changes you have made, need to be under the same AGPLv3 license. If you simply use our code without modifications, just pointing them to this repository is enough.
* Added support for Argon2 key derivation on the clients. To enable it for your account, make sure all your clients are using version v2023.2.0 or greater, then go to account settings > security > keys, and change the algorithm from PBKDF2 to Argon2id.
* Added support for Argon2 key derivation for the admin page token. To update your admin token to use it, check the wiki

[1.12.1]
* Update Vaultwarden to 1.28.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.28.1)
* Decode knowndevice X-Request-Email as base64url with no padding by @jjlin in https://github.com/dani-garcia/vaultwarden/pull/3376
* Fix abort on password reset mail error by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/3390
* support /users/<uuid>/invite/resend admin api by @nikolaevn in https://github.com/dani-garcia/vaultwarden/pull/3397
* always return KdfMemory and KdfParallelism by @stefan0xC in https://github.com/dani-garcia/vaultwarden/pull/3398
* Fix sending out multiple websocket notifications by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/3405
* Revert setcap, update rust and crates by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/3403

[1.13.0]
* Update Vaultwarden to 1.29.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.29.0)
* Mobile Client push notification support, see #3304 thanks @GeekCornerGH!
* Web-Vault updated to v2023.5.0 (v2023.5.1 does not add any improvements for us)
* The latest Bitwarden Directory Connector can be used now (v2022.11.0)
* check if reset password policy is enabled by @stefan0xC in #3427
* WebSockets via Rocket's Upgrade connection by @BlackDex in #3404
* Several config and admin interface fixes by @BlackDex in #3436

[1.13.1]
* Update Vaultwarden to 1.29.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.29.1)
* feat: Add support for forwardemail by @GeekCornerGH in #3686
* Fix some `external_id` issues by @BlackDex in #3690
* Remove debug code during attachment download by @BlackDex in #3704

[1.13.2]
* Rename `/app/data/config.env` to `/app/data/env.sh`

[1.13.3]
* Update Vaultwarden to 1.29.2
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.29.2)
* Fix .env.template file by @BlackDex in #3734
* Fix UserOrg status during LDAP Import by @BlackDex in #3740
* Update images to Bookworm and PQ15 and Rust v1.71 by @BlackDex in #3573
* Implement "login with device" by @quexten in #3592
* chore: Bump web vault to v2023.7.1 and bump Rust by @GeekCornerGH in #3769
* Optimized Favicon downloading by @BlackDex in #3751
* add UserDecryptionOptions to login response by @stefan0xC in #3813
* add new secretsmanager plan for web-v2023.8.x by @stefan0xC in #3797

[1.13.4]
* Fix database table collation and charset

[1.13.5]
* Update Vaultwarden to 1.29.2
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.29.2)
* Fix .env.template file by @BlackDex in #3734
* Fix UserOrg status during LDAP Import by @BlackDex in #3740
* Update images to Bookworm and PQ15 and Rust v1.71 by @BlackDex in #3573
* Implement "login with device" by @quexten in #3592
* chore: Bump web vault to v2023.7.1 and bump Rust by @GeekCornerGH in #3769
* Optimized Favicon downloading by @BlackDex in #3751
* add UserDecryptionOptions to login response by @stefan0xC in #3813
* add new secretsmanager plan for web-v2023.8.x by @stefan0xC in #3797

[1.14.0]
* Update base image to 4.2.0

[1.15.0]
* Fix issue where mobile clients are crashing when adding new entries

[1.16.0]
* Update Vaultwarden to 1.30.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.0)
* Added passkey support, allowing the browser extensions to store and use your passkeys, make sure the extension is updated to version 2023.10.0 or newer for passkey support.
* Updated web vault to 2023.10.0.
* Fixed crashes when trying to create/edit a cipher in the mobile applications.

[1.16.1]
* Update web vault to 2023.10.0

[1.16.2]
* Update Vaultwarden to 1.30.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.1)
* Disable autofill-v2 by @BlackDex in #4056
* Add Protected Actions Check by @BlackDex in #4067

[1.16.3]
* Update Vaultwarden to 1.30.2
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.2)
* Prevent generating an error during ws close by @BlackDex in #4127
* Update Rust, Crates, Profile and Actions by @BlackDex in #4126
* Several small fixes for open issues by @BlackDex in #4143
* Fix the version string by @BlackDex in #4153
* Decrease JWT Refresh/Auth token by @BlackDex in #4163
* Update crates by @BlackDex in #4173
* Add additional build target which optimizes for size by @gladiac in #4096
* Update web-vault to v2023.12.0 by @BlackDex in #4201
* Update Rust and Crates by @BlackDex in #4211
* Fix Single Org Policy check by @BlackDex in #4207
* Allow customizing the featureStates by @PKizzle in #4168

[1.16.4]
* Update Vaultwarden to 1.30.3
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.3)
* fix push device registration by @stefan0xC in #4297
* Fix healthcheck when using .env file by @BlackDex in #4299

[1.16.5]
* Update Vaultwarden to 1.30.5
* Update web vault to 2024.1.2b
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.30.3)
* Update crates to fix new builds by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4308
* Add Kubernetes environment detection by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4290
* Update GHA Workflows by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4309
* Update Rust, crates and web-vault by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4328
* Change the codegen-units for low resources by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4336
* Fix env templateto ensure compatibility with systemd's EnvironmentFile parsing by @seiuneko in https://github.com/dani-garcia/vaultwarden/pull/4315
* Update crates, GHA and a Python script by @BlackDex in https://github.com/dani-garcia/vaultwarden/pull/4357
* fix: web API call for jquery 3.7.1 by @calvin-li-developer in https://github.com/dani-garcia/vaultwarden/pull/4400

[1.17.0]
* Update Vaultwarden to 1.31.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.31.0)
* Initial support for the beta releases of the new native mobile apps
* Removed support for WebSocket traffic on port 3012, as it's been integrated on the main HTTP port for a few releases
* Updated included web vault to 2024.5.1

[1.18.0]
* Update Vaultwarden to 1.32.0
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.0)
* CVE-2024-39924 Fixed via #4715
* CVE-2024-39925 Fixed via #4837
* CVE-2024-39926 Fixed via #4737
* Updated web-vault to v2024.6.2
* Fixed issues with password reset enrollment by rolling back a web-vault commit

[1.18.1]
* Update Vaultwarden to 1.32.1
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.1)
* Fixed syncing/login with native mobile clients
* Added CLI option to backup SQLite database
* Email Template changes regarding invites, 2FA Incomplete logins, and new logins

[1.18.2]
* Update Vaultwarden to 1.32.2
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.2)
* Fixed collection management for managers
* Fix compiling for Windows targets by @​BlackDex in https://github.com/dani-garcia/vaultwarden/pull/5053
* Updates and collection management fixes by @​BlackDex in https://github.com/dani-garcia/vaultwarden/pull/5072
* Fix --version from failing without config by @​BlackDex in https://github.com/dani-garcia/vaultwarden/pull/5055

[1.18.3]
* Update Vaultwarden to 1.32.3
* [Full changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.3)
* Email template for org invites was updated again. The URL got HTML Encoded which resulted in a sometimes non-working URL (#5100)
* Fixed SMTP issues with some providers which send erroneous response to QUIT messages (Like QQ) (Thanks to @paolobarbolini)
* Fixed a long standing collection management issue where collections were not able to be managed via the Password Manager overview

[1.18.4]
* Update vaultwarden to 1.32.4
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.4)
* Added more compatibility fixes for the native mobile apps, datetimes are now formatted without too many decimals.
* Email Template changes to the send emergency access invite. If you have modified this template, make sure to update it with the new changes.
* Update README by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5153

[1.18.5]
* Update vaultwarden to 1.32.5
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.5)
* Added SSH-Key storage support. Currently only usable with Bitwarden Desktop v2024.12.0 and newer.
* Fix if logic error by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5171
* More authrequest fixes by [@&#8203;dani-garcia](https://github.com/dani-garcia) in https://github.com/dani-garcia/vaultwarden/pull/5176
* Add dynamic CSS support by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/4940
* fix hibp username encoding and pw hint check by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5180
* Remove auth-request deletion by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5184
* fix password hint check by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5189

[1.18.6]
* Update vaultwarden to 1.32.6
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.6)
* Fix push not working by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5214
* Fix editing members which have access-all rights by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5213
* Update Rust and crates by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5248
* Update Alpine to version 3.21 by [@&#8203;dfunkt](https://github.com/dfunkt) in https://github.com/dani-garcia/vaultwarden/pull/5256
* Fix another sync issue with native clients by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5259
* Update crates by [@&#8203;dfunkt](https://github.com/dfunkt) in https://github.com/dani-garcia/vaultwarden/pull/5268
* Some Backend Admin fixes and updates by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5272
* [@&#8203;chuangjinglu](https://github.com/chuangjinglu) made their first contribution in https://github.com/dani-garcia/vaultwarden/pull/5224

[1.18.7]
* Update vaultwarden to 1.32.7
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.32.7)
* feat: mask \_smtp_img_src in support string by [@&#8203;tessus](https://github.com/tessus) in https://github.com/dani-garcia/vaultwarden/pull/5281
* Some refactoring, optimizations and security fixes by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5291
* Allow adding connect-src entries by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5293
* Use updated fern instead of patch by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5298

[1.19.0]
* Update vaultwarden to 1.33.0
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.33.0)
* [GHSA-f7r5-w49x-gxm3](https://github.com/dani-garcia/vaultwarden/security/advisories/GHSA-f7r5-w49x-gxm3)
* [GHSA-h6cc-rc6q-23j4](https://github.com/dani-garcia/vaultwarden/security/advisories/GHSA-h6cc-rc6q-23j4)
* [GHSA-j4h8-vch3-f797](https://github.com/dani-garcia/vaultwarden/security/advisories/GHSA-j4h8-vch3-f797)
* Updated web-vault to v2025.1.1
* Added partial *manage* role support for collections
* Manager role is converted to a Custom role with either Manage All Collections or per collection.
* The OCI containers and binaries are *signed* via [GitHub Attestations](https://docs.github.com/en/actions/security-for-github-actions/using-artifact-attestations/using-artifact-attestations-to-establish-provenance-for-builds#verifying-artifact-attestations-with-the-github-cli)
* Add `inline-menu-positioning-improvements` feature flag by [@&#8203;Ephemera42](https://github.com/Ephemera42) in https://github.com/dani-garcia/vaultwarden/pull/5313
* Fix issues when uri match is a string by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5332
* Add TOTP delete endpoint by [@&#8203;Timshel](https://github.com/Timshel) in https://github.com/dani-garcia/vaultwarden/pull/5327
* fix group issue in send_invite by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5321
* Update crates and GHA by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5346
* Refactor the uri match fix and fix ssh-key sync by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5339
* Add partial role support for manager only using web-vault v2024.12.0 by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5219
* Fix issue with key-rotate by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5348
* fix manager role in admin users overview by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5359
* Prevent new users/members to be stored in db when invite fails by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5350
* Update crates and web-vault to v2025.1.0 by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5368
* Allow building with Rust v1.84.0 or newer by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5371
* rename membership and adopt newtype pattern by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5320
* build: raise msrv (1.83.0) rust toolchain (1.84.0) by [@&#8203;tessus](https://github.com/tessus) in https://github.com/dani-garcia/vaultwarden/pull/5374
* Fix an issue with login with device by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5379
* refactor: replace static with const for global constants by [@&#8203;Integral-Tech](https://github.com/Integral-Tech) in https://github.com/dani-garcia/vaultwarden/pull/5260
* Add Attestations for containers and artifacts by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5378
* Fix version detection on bake by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5382
* Simplify container image attestation by [@&#8203;dfunkt](https://github.com/dfunkt) in https://github.com/dani-garcia/vaultwarden/pull/5387
* improve admin invite by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5403
* Add manage role for collections and groups by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5386
* update web-vault to v2025.1.1 and add /api/devices by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5422
* Security fixes by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5438
* only validate SMTP_FROM if necessary by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5442

[1.19.1]
* Update vaultwarden to 1.33.1
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.33.1)
* Icon's not working on the Desktop clients
* Invites not always working
* DUO settings not able to configure
* Manager rights
* Mobile client sync issues fixed
* hide already approved (or declined) auth_requests by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5467
* let invited members access OrgMemberHeaders by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5461
* Make sure the icons are displayed correctly in desktop clients by [@&#8203;WinLinux1028](https://github.com/WinLinux1028) in https://github.com/dani-garcia/vaultwarden/pull/5469
* Fix passwordRevisionDate format by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5477
* add and use new event types by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5482
* Fix Duo Field Names for Web Client by [@&#8203;ratiner](https://github.com/ratiner) in https://github.com/dani-garcia/vaultwarden/pull/5491
* Allow all manager to create collections again by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5488
* Update Rust to 1.84.1 by [@&#8203;dfunkt](https://github.com/dfunkt) in https://github.com/dani-garcia/vaultwarden/pull/5508
* [@&#8203;WinLinux1028](https://github.com/WinLinux1028) made their first contribution in https://github.com/dani-garcia/vaultwarden/pull/5469
* [@&#8203;ratiner](https://github.com/ratiner) made their first contribution in https://github.com/dani-garcia/vaultwarden/pull/5491

[1.19.2]
* Update vaultwarden to 1.33.2
* [Full Changelog](https://github.com/dani-garcia/vaultwarden/releases/tag/1.33.2)
* Update workflows and enhance security by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5537
* Update crates & fix CVE-2025-24898 by [@&#8203;dfunkt](https://github.com/dfunkt) in https://github.com/dani-garcia/vaultwarden/pull/5538
* add bulk-access endpoint for collections by [@&#8203;stefan0xC](https://github.com/stefan0xC) in https://github.com/dani-garcia/vaultwarden/pull/5542
* Fix icon redirect not working on desktop by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5536
* Show assigned collections on member edit by [@&#8203;BlackDex](https://github.com/BlackDex) in https://github.com/dani-garcia/vaultwarden/pull/5556

